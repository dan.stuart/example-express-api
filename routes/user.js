var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send({
    "fistName": "Dan",
    "lastName": "Stuart",
    "email": "dan.stuart@example.com",
  });
});

module.exports = router;
