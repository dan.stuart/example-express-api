var express = require('express');
var router = express.Router();

/* GET post listing. */
router.get('/', function(req, res, next) {
  res.send([{
    "id": 1,
    "created_at": "2022-01-06T12:51:26.208Z",
    "title": "My Post",
    "url": "/blog/my-post/",
    "description": "This is my post",
  }, {
    "id": 2,
    "created_at": "2022-01-05T12:51:26.208Z",
    "title": "My Second Post",
    "url": "/blog/my-second-post/",
    "description": "This is another post",
  }]);
});

// get post
router.get('/:id', function(req, res, next) {
  res.send({
    "id": parseInt(req.params.id),
    "created_at": "2022-01-06T12:51:26.208Z",
    "title": "My Post",
    "url": "/blog/my-post/",
    "description": "This is my post",
    "body": "This is my article body",
  });
});

module.exports = router;
